-- CREATE DATA
CREATE TABLE IF NOT EXISTS cities(
    id   SERIAL PRIMARY KEY,
    name TEXT
);

INSERT INTO cities VALUES (1, 'moscow');
INSERT INTO cities VALUES (2, 'paris');
INSERT INTO cities VALUES (3, 'Berlin');
INSERT INTO cities VALUES (4, 'LONDON');
INSERT INTO cities VALUES (5, 'Luxembourg');
INSERT INTO cities VALUES (6, 'Amsterdam');

SELECT * from cities;

-- LIKE/ILIKE with '_' operator
SELECT * FROM cities WHERE name LIKE 'mo_cow';
SELECT * FROM cities WHERE name LIKE 'Mo_cow';
SELECT * FROM cities WHERE name ILIKE 'Mo_cow';

-- LIKE/ILIKE with '%' operator
SELECT * FROM cities WHERE name LIKE '%bourg';
SELECT * FROM cities WHERE name LIKE '%er%';
SELECT * FROM cities WHERE name LIKE 'Pa%';
SELECT * FROM cities WHERE name ILIKE 'Pa%';



-- Similarity search, trigram extension setup
CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;

SELECT show_trgm('words') AS words,
       show_trgm('word') AS word;

SELECT similarity('words', 'word') AS similarity,
       strict_word_similarity('words', 'word') AS strict_word_similarity, --whole words
       word_similarity('words', 'word') AS word_similarity; --any substring (parts of words)

SELECT similarity('word', 'several words used') AS similarity,
       strict_word_similarity('word', 'several words used') AS strict_word_similarity,
       word_similarity('word', 'several words used') AS word_similarity;

SHOW pg_trgm.similarity_threshold;

SELECT similarity('like', 'likely'), 'like' % 'likely' AS is_over_threshold;
SELECT similarity('different', 'diverse'), 'different' % 'diverse' AS is_over_threshold;

SELECT 'like' % 'likely' AS is_similar,
       'like' %> 'likely' AS is_strict_similar,
       'like' %>> 'likely' AS is_strict_word_similar;

-- distance between words operator
select 'word' <-> 'words', similarity('word','words'), 1 - similarity('word','words');

SHOW pg_trgm.similarity_threshold;
SHOW pg_trgm.word_similarity_threshold;
SHOW pg_trgm.strict_word_similarity_threshold;

SET pg_trgm.similarity_threshold = 0.6;
SET pg_trgm.word_similarity_threshold = 0.7;
SET pg_trgm.strict_word_similarity_threshold = 0.8;

-- Grand Unified Configuration (GUC) Parameter. Can be set:
-- 1) globally in postgresql.conf
-- 2) per DB (ALTER DATABASE ... SET ... )
-- 3) per USER (ALTER ROLE ... SET ... )
-- 4) per session (SET ...)
-- 5) per function (CREATE FUNCTION ... SET ... )
-- 6) per subtransaction (SET LOCAL)

-- trigrams contain alpha-numerics only:
-- pg_trgm ignores non-word characters (non-alphanumerics) when extracting trigrams from a string.
-- Each word is considered to have two spaces prefixed and one space suffixed when determining the set of trigrams contained in the string.
-- For example, the set of trigrams in the string “cat” is “ c”, “ ca”, “cat”, and “at ”.
SELECT show_trgm('c,a.t'), array_length(show_trgm('c,a.t'), 1),
       show_trgm('cat'), array_length(show_trgm('cat'), 1);



-- Difference between 3 similarity functions
select show_trgm('word'); --  {  w, wo,wor,ord,rd } - 5
select show_trgm('words'); -- {  w, wo,wor,ord,rds,ds } - 6
select show_trgm('two words'); -- {  t, tw,two, wo,  w, wo,wor,ord,rds,ds } - 10

-- similarity
-- counts equal trigrams divided by total number on unique trigrams
select similarity('words', 'word'); -- 0.5714286 = 4/7
select similarity('word', 'two words');
select similarity('word', 'two words'), similarity('two words', 'word'); -- symmetric function

-- word_similarity
-- Returns a number that indicates the greatest similarity between the set of trigrams in the first string
-- and any continuous extent of an ordered set of trigrams in the second string.
select word_similarity('word', 'two words'); -- 0.8 = 4/5
select word_similarity('word', 'two wordsblabla'); -- 0.8 = 4/5
select word_similarity('word', 'two words'), word_similarity('two words', 'word'); -- not symmetric
-- WORD_SIMILARITY is more suitable for finding the similarity for parts of words

-- strict_word_similarity
-- Same as word_similarity, but forces extent boundaries to match word boundaries. Since we don't have cross-word trigrams,
-- this function actually returns greatest similarity between first string and any continuous extent of words of the second string.
select strict_word_similarity('word', 'two words'); -- 0.5714286 = 4/7
select strict_word_similarity('word', 'two wordsblabla'); -- 0.333(3)
select strict_word_similarity('word', 'two words'), strict_word_similarity('two words', 'word'); -- not symmetric
select strict_word_similarity('word', 'two words'), similarity('word', 'words');
-- STRICT_WORD_SIMILARITY function is useful for finding the similarity to whole words



-- add fuzzystrmatch extension (Soundex, Levenshtein, Metaphone)
CREATE EXTENSION IF NOT EXISTS fuzzystrmatch WITH SCHEMA public;

SELECT similarity('impressionism', 'expressionism');

--levenshtein(text source, text target) returns int
SELECT levenshtein('impressionism', 'expressionism');
SELECT 1 - levenshtein('impressionism', 'expressionism')::NUMERIC / length('impressionism');

-- levenshtein(text source, text target, int ins_cost, int del_cost, int sub_cost) returns int
SELECT levenshtein('impressionism', 'expressionism', 1, 1, 2);

-- levenshtein_less_equal(text source, text target, int max_d) returns int
SELECT levenshtein_less_equal('similar', 'similarity', 3);
SELECT levenshtein_less_equal('similar', 'similari', 3);
SELECT levenshtein_less_equal('similar', 'similari', 3) <= 3;
SELECT levenshtein('similar', 'similarity');

SELECT levenshtein_less_equal('similar', 'different', 3);
SELECT levenshtein('similar', 'different');



-- Full text search: basic
SELECT to_tsquery('english', 'creative');
SELECT to_tsvector('english', 'Creativity is considered crucial for success');
SELECT to_tsvector('english', 'Creativity is considered crucial for success') @@ to_tsquery('english', 'creative');

-- Full text search: and/or
SELECT to_tsvector('english', 'some phrase to search words in');
SELECT to_tsvector('english', 'some phrase to search words in') @@ to_tsquery('english', 'searching | inexistent') AS or_search;
SELECT to_tsvector('english', 'some phrase to search words in') @@ to_tsquery('english', 'searching & inexistent') AS and_search;

-- Full text search: dictionary
SELECT to_tsquery('Nähmaschine');
SELECT to_tsquery('german', 'Nähmaschine');
SELECT to_tsquery('Räder');
SELECT to_tsquery('english', 'Räder');
SELECT to_tsquery('german', 'Räder');

SELECT cfgname FROM pg_ts_config; -- 23 entries

SELECT to_tsvector('Ich fahre heute mit dem Rad zur Schule');
SELECT to_tsvector('german', 'Ich fahre heute mit dem Rad zur Schule'); -- 'fahr':2 'heut':3 'rad':6 'schul':8

SELECT to_tsvector('german', 'Ich fahre heute mit dem Rad zur Schule') @@ to_tsquery('schulen');
SELECT to_tsquery('schulen');

SELECT to_tsvector('german', 'Ich fahre heute mit dem Rad zur Schule') @@ to_tsquery('german', 'schulen');
SELECT to_tsquery('german', 'schulen');

SELECT to_tsvector('german', 'Ich fahre heute mit dem Rad zur Schule') @@ to_tsquery('german', 'schulen & Räder & fahren');
SELECT to_tsquery('german', 'schulen & Räder & fahren');
