# README #

This README includes all the necessary materials and instructions necessary for starting up a PostgreSQL-based 
infrastructure to dive into various text-search techniques.

### Setting up ###

* Install [Docker](https://www.docker.com)
* Create PostgreSQL Docker container:

```
docker run -d --name text_search_demo \
-e POSTGRES_DB=text_search_demo \
-e POSTGRES_PASSWORD=pass \
-e POSTGRES_USER=user \
-p 5432:5432 \
postgres:14.4-alpine
```

* Copy words dictionary data for ful-text search into Docker container (for MacOS/Unix users). Do not forget to replace `MY_PATH`:

```
cp /usr/share/dict/words /MY_PATH/words.list && \
docker cp /MY_PATH/words.list text_search_demo:/var/lib/postgresql/data/words.list
```
If this option is not available, use the [words.list file](words.list)

```
docker cp /path/to/this/project/words.list text_search_demo:/var/lib/postgresql/data/words.list
```

* You can now go through [theoretical](theory.sql) or [practical](practice.sql) parts in any order.
