-- prepare data for ful-text search (https://stackoverflow.com/a/39583950)

-- for MacOS to add unix words dictionary to get full text search test data from:
-- cp /usr/share/dict/words /MY_PATH/words.list && \
-- docker cp /MY_PATH/words.list my_container_name:/var/lib/postgresql/data/words.list

-- e.g.
-- cp /usr/share/dict/words /Users/oratnikov/words.list && \
-- docker cp /Users/oratnikov/words.list text_search_demo:/var/lib/postgresql/data/words.list

-- NOTICE START: CREATING TEST DATA

-- create test table
CREATE TABLE IF NOT EXISTS influencers(
    id       SERIAL PRIMARY KEY,
    nickname TEXT, --random data
    hashtag  TEXT  --words from dictionary
);

CREATE OR REPLACE FUNCTION public.getRandomWord(array_of_words text[])
    RETURNS text LANGUAGE sql AS
$func$
SELECT array_of_words[random() * (array_length(array_of_words, 1) - 1) + 1]
$func$;

-- generate 1_000_000 records with random md5 nicknames and unix dictionary hashtags, FIXME this step takes 6+ minutes to complete because of getRandomWord()
WITH array_of_words(words_data) AS(
    SELECT string_to_array(pg_read_file('words.list')::text,E'\n')
)
INSERT INTO influencers (nickname, hashtag)
SELECT
    CASE WHEN (random() > 0.5) THEN left(md5(random()::text), 10) ELSE concat_ws('_', left(md5(random()::text), 5), left(md5(random()::text), 5)) END,
    getRandomWord(array_of_words.words_data)
FROM array_of_words, generate_series(1, 1000000);

-- NOTICE END: TEST DATA CREATED

-- check data presence
SELECT count(*) FROM influencers;
SELECT * FROM influencers LIMIT 20;

-- add trgm extension
CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;

-- add unaccent extension
-- ÉÈËÊÀÁÂÃÄÅÉÈËÊÌÍÎÏÌÍÎÏÒÓÔÕÖÙÚÛÜàáâãäåèéêëìíîïòóôõöùúûüÇç
CREATE EXTENSION IF NOT EXISTS unaccent WITH SCHEMA public;

-- examples on unaccent extension
SELECT similarity('hotel', 'Hôtél'); -- 0.3(3)
SELECT similarity('hotel', unaccent('Hôtél')); -- 1

-- functions must be IMMUTABLE to be used in index expressions. Unaccent is not IMMUTABLE. https://stackoverflow.com/a/11007216
CREATE OR REPLACE FUNCTION public.immutable_unaccent(regdictionary, text)
    RETURNS text LANGUAGE c IMMUTABLE STRICT AS
'$libdir/unaccent', 'unaccent_dict';

CREATE OR REPLACE FUNCTION public.i_unaccent(text)
    RETURNS text LANGUAGE sql IMMUTABLE STRICT AS
$func$
SELECT public.immutable_unaccent(regdictionary 'public.unaccent', $1)
$func$;

------- NOTICE => LIKE SEARCH-------

SELECT * FROM influencers LIMIT 20;
EXPLAIN ANALYSE
SELECT * FROM influencers WHERE i_unaccent(nickname) LIKE i_unaccent('08%');

-- btree with text_pattern_ops for left-anchored search patterns (starts with aka autocomplete search)
-- if you need <, >, <=, >= comparisons, also consider adding btree without text_pattern_ops
CREATE INDEX CONCURRENTLY IF NOT EXISTS influencers_unaccent_nickname_btree_text_pattern_ops_idx
    ON influencers (i_unaccent(nickname) text_pattern_ops); --3-4 seconds

EXPLAIN ANALYSE
SELECT * FROM influencers WHERE i_unaccent(nickname) LIKE i_unaccent('08%');

-- does not support right-anchored search
SELECT * FROM influencers LIMIT 20;
EXPLAIN ANALYSE
SELECT * FROM influencers WHERE i_unaccent(nickname) LIKE i_unaccent('%84c');

-- does not support ILIKE search
EXPLAIN ANALYSE
SELECT * FROM influencers WHERE i_unaccent(nickname) ILIKE i_unaccent('e0');

------- NOTICE => SIMILARITY SEARCH + ILIKE INDEX SUPPORT-------

INSERT INTO influencers (nickname, hashtag) VALUES ('ÀÁÂÃÄàáâãä', 'popularity');
INSERT INTO influencers (nickname, hashtag) VALUES ('AAAAAaaaaa', 'fashionable');
SELECT similarity('aaEEEEEEaa', 'AAAAAaaaaa');
SELECT show_limit();

EXPLAIN ANALYSE
SELECT * FROM influencers WHERE i_unaccent(nickname) % i_unaccent('aaEEEEEEaa');

-- gin with gin_trgm_ops for similarity search
CREATE INDEX CONCURRENTLY IF NOT EXISTS influencers_unaccent_nickname_gin_gin_trgm_ops_idx
    ON influencers USING GIN (i_unaccent(nickname) gin_trgm_ops); --20-30 seconds

EXPLAIN ANALYSE
SELECT * FROM influencers WHERE i_unaccent(nickname) % i_unaccent('aaEEEEEEaa');

-- ILIKE is also supported now for both left- and right-anchored search
EXPLAIN ANALYSE
SELECT * FROM influencers WHERE i_unaccent(nickname) ILIKE i_unaccent('e0%');

EXPLAIN ANALYSE
SELECT * FROM influencers WHERE i_unaccent(nickname) ILIKE i_unaccent('%49a');

------- NOTICE => FULL TEXT SEARCH-------

EXPLAIN ANALYSE
SELECT *
FROM influencers
WHERE to_tsvector('english', i_unaccent(hashtag))
          @@ to_tsquery('english', concat_ws('|', i_unaccent('fÄshion'), i_unaccent('pÕpular')));

-- GIN on to_tsvector for full text search
CREATE INDEX CONCURRENTLY IF NOT EXISTS influencers_unaccent_hashtag_gin_to_tsvector_english_idx
    ON influencers USING GIN(to_tsvector('english', i_unaccent(hashtag))); --10 seconds

EXPLAIN ANALYSE
SELECT *
FROM influencers
WHERE to_tsvector('english', i_unaccent(hashtag))
          @@ to_tsquery('english', concat_ws('|', i_unaccent('fÄshion'), i_unaccent('pÕpular')));
